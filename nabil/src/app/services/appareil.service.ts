import { HttpClient } from '@angular/common/http';
import { stringify } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs-compat';
import { EditAppareilComponent } from '../edit-appareil-component/edit-appareil.component';

@Injectable()
export class AppareilService {
  appareilsSubject = new Subject<any[]>();

  constructor(private httpClient: HttpClient) {}
  private appareils = [
    {
      id: 1,
      name: 'Machine à laver',
      status: 'éteint',
    },
    {
      id: 2,
      name: 'Frigo',
      status: 'allumé',
    },
    {
      id: 3,
      name: 'Ordinateur',
      status: 'éteint',
    },
  ];

  getAppareilById(id: number) {
    let appareil = this.appareils[id - 1];
    if (!appareil) return this.appareils[0];
    return appareil;
  }

  emitAppareilSubject() {
    this.appareilsSubject.next(this.appareils.slice());
  }

  switchOnAll() {
    for (let appareil of this.appareils) {
      appareil.status = 'allumé';
    }
    this.emitAppareilSubject();
  }

  switchOffAll() {
    for (let appareil of this.appareils) {
      appareil.status = 'éteint';
      this.emitAppareilSubject();
    }
  }

  switchOnOne(i: number) {
    this.appareils[i].status = 'allumé';
    this.emitAppareilSubject();
  }

  switchOffOne(i: number) {
    this.appareils[i].status = 'éteint';
    this.emitAppareilSubject();
  }

  addAppareil(name: string, status: string) {
    const appa = {
      id: 0,
      name: '',
      status: '',
    };

    appa.name = name;
    appa.status = status;
    appa.id = this.appareils[this.appareils.length - 1].id + 1;
    this.appareils.push(appa);
    this.emitAppareilSubject();
  }

  saveAppareilsToServer() {
    this.httpClient
      .put(
        'https://test-angular-92a8e-default-rtdb.firebaseio.com/appareils.json',
        this.appareils
      )
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  getAppareilsFromServer() {
    this.httpClient
      .get<any[]>(
        'https://test-angular-92a8e-default-rtdb.firebaseio.com/appareils.json'
      )

      .subscribe(
        (response) => {
          this.appareils = response;
          this.emitAppareilSubject();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
}
